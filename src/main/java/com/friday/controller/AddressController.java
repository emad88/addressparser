package com.friday.controller;

import com.friday.datatransferobject.AddressDTO;
import com.friday.exception.AddressException;
import com.friday.service.AddressService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/address")
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AddressDTO parseAddress(@RequestBody String address) throws AddressException {
        return addressService.parseStringAddress(address);
    }

}
