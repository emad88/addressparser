package com.friday.datatransferobject;

public class AddressDTO {
    private String streetName;
    private String streetNumber;

    private AddressDTO(){}

    private AddressDTO(String streetName, String streetNumber){
        this.streetName = streetName;
        this.streetNumber = streetNumber;
    }

    public static AddressDTOBuilder newBuilder() { return new AddressDTOBuilder();}

    public String getStreetName() {
        return streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public static class AddressDTOBuilder{
        private String streetName;
        private String streetNumber;

        public AddressDTOBuilder setStreetName (String streetName){
            this.streetName = streetName;
            return this;
        }

        public AddressDTOBuilder setStreetNumber (String streetNumber){
            this.streetNumber = streetNumber;
            return this;
        }

        public AddressDTO createAddressDTO(){ return new AddressDTO(streetName, streetNumber);}
    }
}
