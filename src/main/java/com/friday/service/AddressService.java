package com.friday.service;

import com.friday.datatransferobject.AddressDTO;
import com.friday.exception.AddressException;

public interface AddressService {
    AddressDTO parseStringAddress(String address) throws AddressException;
}
