package com.friday.service;

import com.friday.datatransferobject.AddressDTO;
import com.friday.exception.AddressException;
import com.friday.util.StringUtil;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    private String streetName;
    private String streetNumber;

    @Override
    public AddressDTO parseStringAddress(String address) throws AddressException {

        // save all the words in a string array without comma and space
        String[] words = address.replaceAll("^[,\\s]+", "").split("[,\\s]+");
        streetNumber = new String();
        streetName = new String();
        AddressDTO addressDTO;
        int count = StringUtil.numberOfIntegers(words);
        switch (count) {
            case 1:
                addressDTO = processNormalCases(words);
                break;
            case 2:
                addressDTO = processBonusCase(words);
                break;
            default:
                throw new AddressException("so many numbers in the address");
        }

        return addressDTO;
    }

    private AddressDTO processNormalCases(String[] words) {
        for (String word : words)
            if (StringUtil.isInt(word)) {
                streetNumber = word;
            } else if (StringUtil.isLetter(word)) {
                streetNumber += " " + word;
            } else {
                streetName += " " + word;
            }

        return getAddressDTO();
    }

    private AddressDTO processBonusCase(String[] words) {
        boolean isFirstNumber = true;
        boolean isInt;
        for (String word : words) {
            isInt = StringUtil.isInt(word);
            if (isInt && isFirstNumber) {
                streetName += " " + word;
                isFirstNumber = false;
            } else if (isInt) {
                streetNumber += " " + word;
            } else if (!isInt && isFirstNumber) {
                streetName += " " + word;
            } else if (!isInt && !isFirstNumber) {
                streetNumber += " " + word;
            }
        }

        return getAddressDTO();
    }


    private AddressDTO getAddressDTO() {
        AddressDTO.AddressDTOBuilder addressDTOBuilder = AddressDTO.newBuilder()
                .setStreetName(streetName.trim())
                .setStreetNumber(streetNumber.trim());

        return addressDTOBuilder.createAddressDTO();
    }
}
