package com.friday.util;

public final class StringUtil {

    public static boolean isInt(String word) {
        return Character.isDigit(word.charAt(0));
    }

    public static boolean isLetter(String word) {
        if (word.length() == 1 && !isInt(word))
            return true;
        return false;
    }

    public static int numberOfIntegers(String[] words) {
        int count = 0;
        for (String word : words) {
            if (Character.isDigit(word.charAt(0)))
                count++;
        }
        return count;

    }
}
