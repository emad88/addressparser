package com.friday.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Wrong Address/Input address cannot get processed...")
public class AddressException extends Exception {

    public AddressException(String message) {
        super(message);
    }
}
