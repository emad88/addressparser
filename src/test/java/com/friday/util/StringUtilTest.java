package com.friday.util;

import org.junit.Assert;
import org.junit.Test;


public class StringUtilTest {
    @Test
    public void isIntTest() {
        Assert.assertEquals(true, StringUtil.isInt("12B"));
        Assert.assertEquals(true, StringUtil.isInt("14"));
        Assert.assertEquals(false, StringUtil.isInt("C12"));
    }

    @Test
    public void isLetterTest() {
        Assert.assertEquals(false, StringUtil.isLetter("1"));
        Assert.assertEquals(true, StringUtil.isLetter("B"));
        Assert.assertEquals(true, StringUtil.isLetter("b"));
        Assert.assertEquals(false, StringUtil.isLetter("bb"));
    }

    @Test
    public void numberOfIntegerInStringTest() {
        String[] words = {"1", "strasse", "12"};
        Assert.assertEquals(2, StringUtil.numberOfIntegers(words));
        words = new String[]{"strasse"};
        Assert.assertEquals(0, StringUtil.numberOfIntegers(words));
    }


}
