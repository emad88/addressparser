package com.friday.controller;

import com.friday.AddressParserApp;
import com.friday.exception.AddressException;
import com.friday.service.AddressService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressParserApp.class)
@ComponentScan("com.friday")
@EnableAutoConfiguration
@WebAppConfiguration
public class AddressControllerTest {
    private static final Logger log = LoggerFactory.getLogger(AddressControllerTest.class);

    private static final String URL = "/v1/address";

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private AddressService addressService;

    @Before
    public void setup() throws Exception {
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void ParseAddressTestCase1() throws Exception {
        String address = "Winterallee 3";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("Winterallee")))
                .andExpect(jsonPath("$.streetNumber", is("3")));
    }

    @Test
    public void ParseAddressTestCase2() throws Exception {
        String address = "Blaufeldweg 123B";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("Blaufeldweg")))
                .andExpect(jsonPath("$.streetNumber", is("123B")));
    }

    @Test
    public void ParseAddressTestCase3() throws Exception {
        String address = "Am Bächle 23";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("Am Bächle")))
                .andExpect(jsonPath("$.streetNumber", is("23")));
    }

    @Test
    public void ParseAddressTestCase4() throws Exception {
        String address = "Auf der Vogelwiese 23 b";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("Auf der Vogelwiese")))
                .andExpect(jsonPath("$.streetNumber", is("23 b")));
    }

    @Test
    public void ParseAddressTestCase5() throws Exception {
        String address = "4, rue de la revolution";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("rue de la revolution")))
                .andExpect(jsonPath("$.streetNumber", is("4")));
    }

    @Test
    public void ParseAddressTestCase6() throws Exception {
        String address = "200 Broadway Av";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("Broadway Av")))
                .andExpect(jsonPath("$.streetNumber", is("200")));
    }

    @Test
    public void ParseAddressTestCase7() throws Exception {
        String address = "Calle Aduana, 29";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("Calle Aduana")))
                .andExpect(jsonPath("$.streetNumber", is("29")));
    }

    @Test
    public void ParseAddressTestCase8() throws Exception {
        String address = "Calle 39 No 1540";
        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.streetName", is("Calle 39")))
                .andExpect(jsonPath("$.streetNumber", is("No 1540")));
    }

    @Test(expected = AddressException.class)
    public void ParseAddressTestCaseException() throws Exception {
        String address = "Calle 39 No 1540 17";
        when(addressService.parseStringAddress(address))
             .thenThrow(new AddressException("wrong Address"));

        mockMvc.perform(post(URL)
                .content(address)
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }
}

