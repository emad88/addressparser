# README #

The project is based on the following technologies:

* Java 1.8
* Spring MVC with Spring Boot
* Maven
* Intellij as IDE 
* Swagger UI for documentation

### About the solution ###

* Parsing string of addresses is still one the problems in the market, and there are many companies working on it. This solution just covers the possible cases that were required by the challenge. 

### How to run it? ###

* Clone the repository in your local pc, run the AddressParserApp
* browse http://localhost:8080 
* Open address-controller link and insert the address 
* controller will return streetName and streetNumber in a json format
* for simplicity you can also run the jar file under jar folder in the terminal: java -jar addressParser.jar



### Covered cases: ###

* Winterallee 3
* Blaufeldweg 123B
* Auf der Vogelwiese 23 b
* 4, rue de la revolution
* 200 Broadway Av
* Calle Aduana, 29
* Calle 39 No 1540